#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Interface Graphique
*******************

Permet de géré les fenetres.
Gère aussi certain traitement car c'est le thread principal
"""

import sys,os, smartcard


cwd = os.path.dirname(os.path.realpath(__file__))

def get_base_dir():
    """
    Fonction qui permet de récupéré le bon dossier ou l'exécuatable ou script est éxécuté
    """
    if getattr(sys,"frozen",False):
        # If this is running in the context of a frozen (executable) file,
        # we return the path of the main application executable
        return os.path.dirname(os.path.abspath(sys.executable))+"/gui"
    else:
        # If we are running in script or debug mode, we need
        # to inspect the currently executing frame. This enable us to always
        # derive the directory of main.py no matter from where this function
        # is being called
        return os.path.dirname(os.path.realpath(__file__))
        thisdir = os.path.dirname(inspect.getfile(inspect.currentframe()))
        return os.path.abspath(os.path.join(thisdir, os.pardir))

cwd = get_base_dir()

sys.path.append( "../" )
sys.path.append( os.path.dirname(cwd) )

import pygtk ,inspect ,sys,gobject,carte
pygtk.require('2.0')
import gtk, glib
from threading import Thread

import SQL_fake,test_SQL

gtk.window_set_default_icon_from_file(cwd+"/icon.ico")

class HelloWorld:
    """Classe pour tester le GUI"""
    #:.. warning::
    #:    Ne pas utiliser car non sûre


    # This is a callback function. The data arguments are ignored
    # in this example. More on callbacks below.
    def hello(self, widget, data=None):
        print "Hello World"

    def delete_event(self, widget, event, data=None):
        # If you return FALSE in the "delete_event" signal handler,
        # GTK will emit the "destroy" signal. Returning TRUE means
        # you don't want the window to be destroyed.
        # This is useful for popping up 'are you sure you want to quit?'
        # type dialogs.
        print "delete event occurred"

        # Change FALSE to TRUE and the main window will not be destroyed
        # with a "delete_event".
        return False

    def destroy(self, widget, data=None):
        print "destroy signal occurred"
        gtk.main_quit()

    def __init__(self):
        # create a new window
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

        # When the window is given the "delete_event" signal (this is given
        # by the window manager, usually by the "close" option, or on the
        # titlebar), we ask it to call the delete_event () function
        # as defined above. The data passed to the callback
        # function is NULL and is ignored in the callback function.
        self.window.connect("delete_event", self.delete_event)

        # Here we connect the "destroy" event to a signal handler.
        # This event occurs when we call gtk_widget_destroy() on the window,
        # or if we return FALSE in the "delete_event" callback.
        self.window.connect("destroy", self.destroy)

        # Sets the border width of the window.
        self.window.set_border_width(10)

        # Creates a new button with the label "Hello World".
        self.button = gtk.Button("Hello World")

        # When the button receives the "clicked" signal, it will call the
        # function hello() passing it None as its argument.  The hello()
        # function is defined above.
        self.button.connect("clicked", self.hello, None)

        # This will cause the window to be destroyed by calling
        # gtk_widget_destroy(window) when "clicked".  Again, the destroy
        # signal could come from here, or the window manager.
        self.button.connect_object("clicked", gtk.Widget.destroy, self.window)

        # This packs the button into the window (a GTK container).
        self.window.add(self.button)

        # The final step is to display this newly created widget.
        self.button.show()

        # and the window
        self.window.show()

    def main(self):
        # All PyGTK applications must have a gtk.main(). Control ends here
        # and waits for an event to occur (like a key press or mouse event).
        gtk.main()

# If the program is run directly or passed as an argument to the python
# interpreter then create a HelloWorld instance and show it
class Login:
    """Fenetre de connection"""

    #:Utilise le module carte.
    def delete_event(self, widget, event, data=None):
        # If you return FALSE in the "delete_event" signal handler,
        # GTK will emit the "destroy" signal. Returning TRUE means
        # you don't want the window to be destroyed.
        # This is useful for popping up 'are you sure you want to quit?'
        # type dialogs.
        print "delete event occurred"

        # Change FALSE to TRUE and the main window will not be destroyed
        # with a "delete_event".
        return False

    def destroy(self, widget, data=None):
        #:Détruit cette instance
        print "destroy signal occurred"
        self.carteth.work = False
        self.carteth.join()

    def __init__(self):
        #:Initialisation de la classe

        self.interface = gtk.Builder()
        #print(cwd+'/login.glade '+ str(os.path.isfile(cwd+'/login.glade')) )
        try:
            self.interface.add_from_file(cwd+'/login.glade')
        except glib.GError:
            md = gtk.MessageDialog(None,
            gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR,
            gtk.BUTTONS_CLOSE, "Erreur de chargement de GUI.\nLe programme va quitter.\n"+cwd+'/login.glade')
            md.run()
            md.destroy()
            os._exit(2) #The system cannot find the file specified.
        self.interface.connect_signals(self)
        
        # When the window is given the "delete_event" signal (this is given
        # by the window manager, usually by the "close" option, or on the
        # titlebar), we ask it to call the delete_event () function
        # as defined above. The data passed to the callback
        # function is NULL and is ignored in the callback function.
        #self.interface.connect("delete_event", self.delete_event)
    
        # Here we connect the "destroy" event to a signal handler.  
        # This event occurs when we call gtk_widget_destroy() on the window,
        # or if we return FALSE in the "delete_event" callback.
        #self.interface.connect("destroy", self.destroy)

        self.img = self.interface.get_object("image1")
        self.img.set_from_file(cwd+'/leocarte.png')


        self.spin = self.interface.get_object("spinner1")
        self.label_cardstatus = self.interface.get_object("label_cardstatus")
        self.login = self.interface.get_object("login")

        self.carteth = carte.GTKThread()
        self.carteth.connect("card_error",self.on_card_error)
        self.carteth.connect("card",self.on_card)
        self.carteth.connect("card_long",self.on_card_long)
        self.carteth.start()

    def on_card_error(self,truc):
        #:Evenement provoquer par une erreur
        print "erreur carte"
        md = gtk.MessageDialog(None,
        gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR,
        gtk.BUTTONS_CLOSE, "Erreur Carte")
        md.run()
        md.destroy()
        
    def on_card(self,*args):
        """Evenement carte valide
        """
        print "carte"
        sn = carte.convertSN(args[1:])
        md = gtk.MessageDialog(None,
        gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO,
        gtk.BUTTONS_CLOSE, "Carte = "+ str(args[1])+ str(args[2])+ str(args[3])+ str(args[4])+ str(args[5])+ str(args[6])+ str(args[7])+"\nConvertie = "+str(sn))
        md.run()
        md.destroy()
        if SQL_fake.get_level(None) :
            self.destroy(self,None)
            #print dir(self.login)
            self.login.hide()
            Main(SQL_fake.get_token(None))

    def on_card_long(self,*args):
        """Evenement carte valide
        
        :warning Ne marche pas. Python n'arrive pas a convertir un long vers gocbject.TYPE_ULONG
        """
        print "carte"
        md = gtk.MessageDialog(None,
        gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_INFO,
        gtk.BUTTONS_CLOSE, "Carte SN= "+ str(args[1]))
        #gtk.BUTTONS_CLOSE, "Carte = "+ str(args[1])+ str(args[2])+ str(args[3])+ str(args[4])+ str(args[5])+ str(args[6])+ str(args[7]))
        md.run()
        md.destroy()
        if SQL_fake.get_level(None) :
            self.destroy(self,None)
            #print dir(self.login)
            self.login.hide()
            Main(SQL_fake.get_token(None))


    def on_mainWindow_destroy(self, widget):
        self.carteth.work = False
        self.carteth.join()
        gtk.main_quit()


    def on_button1_clicked(self, widget):
        #f = Loading()
        #f.main()
        #print(dir(self.spin))
        if(self.spin.get_visible()):
            self.spin.hide()
        else:
            self.spin.show()

    def gtk_main_quit(self,*args):
        self.carteth.work = False
        self.carteth.join()
        gtk.main_quit()

    def main(self):
        #:Boucle GTK
        gtk.main()

class Loading:
    #:Fenetre de chargement

    def __init__(self):
        interface = gtk.Builder()
        interface.add_from_file(cwd+'/loading.glade')
        interface.connect_signals(self)

    def main(self):
        #:Boucle GTK
        gtk.main()




class Main:
    #:Fenêtre principal

    def __init__(self,token):
        self.liststore1 = gtk.ListStore(str, str, str)
        self.token = token
        interface = gtk.Builder()
        interface.add_from_file(cwd+'/main.glade')
        interface.connect_signals(self)
        self.win = interface.get_object("windowMain")
        self.win.set_title(self.win.get_title()+" - "+ SQL_fake.get_name(token))
        #self.aboutwin = interface.get_object("aboutdialog1")

        self.treeView = interface.get_object("treeview1")
        
        rendererText = gtk.CellRendererPixbuf()
        column = gtk.TreeViewColumn("état", rendererText, stock_id=0)
        column.set_sort_column_id(0)
        self.treeView.append_column(column)
        
        rendererText = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Nom - Prénom", rendererText, text=1)
        column.set_sort_column_id(1)
        self.treeView.append_column(column)

        rendererText = gtk.CellRendererText()
        column = gtk.TreeViewColumn("id", rendererText, text=2)
        column.set_sort_column_id(2)
        self.treeView.append_column(column)
        
        self.carteth = carte.GTKThread()
        self.carteth.connect("card_error",self.on_card_error)
        self.carteth.connect("card",self.on_card)
        self.carteth.start()
        
        try:
            test_SQL.__init__()
        except MySQLdb.Error as e:
            self.msg_error("ERREUR SQL!!!!\n"+repr(e)+"\nLe programe va quitter!")
            #sys.exit(-1)
            self.gtk_main_quit()
        
        self.winetu = interface.get_object("dialogcreateetu")
        self.winetu.connect("delete-event",self.on_create_card_etu_close)
        self.add_card = False
        self.card_sn = ('','')
        self.label_sn = interface.get_object("label_sn")
        self.winetu_entry_nom = interface.get_object("entry_nom")
        self.winetu_entry_prenom = interface.get_object("entry_prenom")
        
        self.diagExamNew = interface.get_object("dialogExamNew")
        self.diagExamNew.connect("delete-event",self.on_create_diagExamNew_close)
        self.exam_matiere = interface.get_object("entry_matiere")
        self.exam_promo = interface.get_object("entry_promo")
        self.exam_date = interface.get_object("calendar")
        self.exam_heur = interface.get_object("spinbutton_hour")
        self.exam_min = interface.get_object("spinbutton_min")
        

    def main(self):
        #:Boucle GTK
        gtk.main()

    def on_imagemenuitem_aide_activate(self, widget):
        #:Création de la fenetre à propos
        about = gtk.AboutDialog()
        about.set_program_name("Pointage étudiant")
        about.set_version("0.1.0 (alpha) Maquette")
        about.set_copyright("2015 (c) Amandin Paquet - Thomas Portassau")
        about.set_comments("Logiciel qui permet de pointer les étudiants grâce à la léocarte")
        about.set_website("http://www.unicaen.fr")
        about.set_logo(gtk.gdk.pixbuf_new_from_file(cwd+"/icon_512.png"))
        about.set_authors("Amandin Paquet - Thomas Portassau")
        about.set_documenters("Doc")
        about.set_license("license")
        about.run()
        about.destroy()
        
    def on_create_card_etu_activate(self, widget):
        """Affichage fenetre pour rajouter un étudiant"""
        self.add_card = True
        self.winetu.show()        
    
    
    def on_cancel_clicked(self, widget):
        """Fonction appelé quand le bouton annuler est appuyé"""
        self.on_create_card_etu_close(self, widget)
        
    def on_create_card_etu_close(self, *args):
        """Cache la fenetre pour rajouter un étudiant"""
        print"on_create_card_etu_close"
        self.winetu.hide()
        self.add_card = False
        self.card_sn = ('','')
        return True
    
    def on_add_clicked(self, *args):
        print "add"
        
        #Vérification du fuormulaire
        if( len(self.winetu_entry_nom.get_text()) ==0):
            self.msg_error("Erreur Nom")
            return
        
        if( len(self.winetu_entry_prenom.get_text()) ==0):
            self.msg_error("Erreur prénom")
            return
        
        if( not( type(self.card_sn[0]) is long )):
            self.msg_error("Erreur pas de carte")
            return
        
        test_SQL.add_etudiant(self.card_sn[0], self.winetu_entry_nom.get_text(), self.winetu_entry_prenom.get_text(), "2000-01-01 00:00:00", "Null", 0, "Null", "Null", "Null")
        self.on_create_card_etu_close(*args)
        
    def msg_error(self,msg):
        """Affiche un message d'erreur
        
        :param msg: message à afficher
        :type msg: str"""
        if( not(type(msg) is str)):
            raise TypeError
        else:
            md = gtk.MessageDialog(None,
            gtk.DIALOG_DESTROY_WITH_PARENT, gtk.MESSAGE_ERROR,
            gtk.BUTTONS_CLOSE, msg)
            md.set_title("Message d'erreur")
            md.run()
            md.destroy()
        
    def on_card_error(self,truc):
        #:Evenement provoquer par une erreur
        print "erreur carte"
        self.msg_error("erreur carte")

    def on_card(self,*args):
        #:Evenement carte valide
        print "carte"
        if self.add_card:
            self.card_sn = (carte.convertSN(args[1:]) , args[1:])
            self.label_sn.set_text( str(self.card_sn[0]) + " " + str(self.card_sn[1]))
        else:
            sn = carte.convertSN(args[1:])
            #patate=gtk.gdk.pixbuf_new_from_file(cwd+"/icon_512.png")
            etu = test_SQL.pointage_leo(sn,None)
            self.liststore1.append([gtk.STOCK_NEW, etu[1]+" "+etu[2], str(sn) ])
            self.treeView.set_model(self.liststore1)

    def on_menu_exam_new_activate(self, *args ):
        self.diagExamNew.show()
        
    def on_cancelexam_clicked(self, *args):
        self.on_create_diagExamNew_close(*args)
    
    def on_create_diagExamNew_close(self, *args):
        """Cache la fenetre pour rajouter un éxamen"""
        #print"on_create_card_etu_close"
        self.diagExamNew.hide()
        return True
    
    def on_addexamen_clicked(self, *args):
        print "add EXAM"
        
        #Vérification du formulaire
        if( len(self.exam_matiere.get_text()) ==0):
            self.msg_error("Erreur Nom")
            return
        
        if( len(self.exam_promo.get_text()) ==0):
            self.msg_error("Erreur prénom")
            return
        
        #print "promo=",self.exam_promo.get_text()," matière=",self.exam_matiere.get_text()," Date=",self.exam_date.get_date()," hour=",self.exam_heur.get_value_as_int(),":",self.exam_min.get_value_as_int()
        date_t = self.exam_date.get_date()
        date = str(date_t[0])+"-"+str(date_t[1])+"-"+str(date_t[2])+" "+str(self.exam_heur.get_value_as_int())+":"+str(self.exam_min.get_value_as_int())+":00"
        test_SQL.add_examen(self.exam_matiere.get_text(), date, "1", self.exam_promo.get_text())
        self.on_create_diagExamNew_close(*args)
        
    def on_menu_exam_open_activate(self, widget):
        print"on_menu_exam_open_activate"

    def on_imagemenuitem5_activate(self, widget):
        print "on_imagemenuitem5_activate"
        self.gtk_main_quit()
        
    def gtk_main_quit(self, *args):
        print "gtk_main_quit"
        import threading
        self.carteth.work = False
        self.carteth.join()
        self.carteth=0
        if threading.active_count() > 1:
            print("des thread sont toujours actif.") 
            for t in threading.enumerate():
                if t is not  threading.current_thread():
                    print(t)
        gtk.main_quit()
        

gobject.threads_init()

class thread(Thread):
    #:Gui Thread
    #:@deprecated Ne sustout pas utiliser

    def __init__(self):
        Thread.__init__(self)
        self.glog = Login()

    def run(self):
        gtk.main()



if __name__ == "__main__":
    #hello = HelloWorld1()
    #hello.main()
    Login()
    gtk.main()
    #app=Main()
    #app.main()
