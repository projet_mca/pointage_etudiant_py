#!/usr/bin/env python
if __name__ == "__main__":
	from smartcard.System import readers

	# define the APDUs used in this script
	COMMAND = [0xFF, 0xCA, 0x00, 0x00, 0x00,]

	# get all the available readers
	r = readers()
	print "Available readers:", r

	reader = r[0]
	print "Using:", reader

	connection = reader.createConnection()
	connection.connect()

	data, sw1, sw2 = connection.transmit(COMMAND)
	for d in data:
		print hex(d),
	print "\t",
	print "Command: %02X %02X" % (sw1, sw2)