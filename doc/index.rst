.. Pointage étudiant.py documentation master file, created by
   sphinx-quickstart on Mon Nov 16 11:03:07 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Bienvenue sur la documentation de Pointage étudiant.py
======================================================

Contenue:

.. toctree::
   :maxdepth: 2
   
   gui.rst
   carte.rst
   SQLdb.rst

   
Test?



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

