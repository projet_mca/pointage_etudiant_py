#!/usr/bin/env python
# -*- coding: utf-8 -*-

from sys import exc_info 
from threading import Thread, Event 
from time import sleep

from smartcard.System import readers 
from smartcard.Exceptions import CardRequestTimeoutException 
from smartcard.Observer import Observer 
from smartcard.Observer import Observable 

from smartcard.CardType import AnyCardType 
from smartcard.CardRequest import CardRequest

ATR_Leocarte = [59, 129, 128, 1, 128, 128]

def isleocarte(atr):
    if not(len(atr) == 6):
        return False
    for i in range(0,6):
        if not(atr[i] == ATR_Leocarte[i]):
            return False
    return True
    
def selectreaderfromname(name):
    rs = readers()
    for r in rs:
        if r.name == name:
            return r
    return -1
    
COMMAND = [0xFF, 0xCA, 0x00, 0x00, 0x00,]

if __name__ == "__main__": 
    from smartcard.CardMonitoring import CardMonitor 
    print 'insert or remove cards in the next 10 seconds' 
    
    # a simple card observer that prints added/removed cards 
    class printobserver(Observer): 
    
        def __init__(self, obsindex): 
            self.obsindex = obsindex 
        
        def update(self, observable, (addedcards, removedcards)): 
            print "%d - added:   " % self.obsindex, addedcards 
            print "%d - removed: " % self.obsindex, removedcards
            if(len(addedcards)):
                for ac in addedcards:
                    if(isleocarte(ac.atr)):
                        print "atr léo"
                        lecteur = selectreaderfromname(ac.reader)
                        if type(lecteur) is int and lecteur == -1:
                            print "Readers not found!!!"
                        else:
                            connection = lecteur.createConnection()
                            connection.connect()
                            data, sw1, sw2 = connection.transmit(COMMAND)
                            for d in data:
                                print hex(d),
                            print "\t",
                            print "Command: %02X %02X" % (sw1, sw2)
                    else:
                        print "not leo"
    
    class testthread(Thread): 
    
        def __init__(self, obsindex): 
            Thread.__init__(self) 
            self.readermonitor = CardMonitor() 
            self.obsindex = obsindex 
            self.observer = None 
        
        def run(self): 
            while 1:
                self.observer = printobserver(self.obsindex) 
                self.readermonitor.addObserver(self.observer) 
                sleep(100) 
                self.readermonitor.deleteObserver(self.observer) 
    
    t1 = testthread(1) 
    t1.start()
    t1.join()