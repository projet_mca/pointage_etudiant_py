#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Carte
*****
Classe qui gère les lecteurs de carte et aussi les cartes connecté.
"""

from sys import exc_info 
from threading import Thread, Event 
from time import sleep

from smartcard.System import readers 
import smartcard.Exceptions
from smartcard.Observer import Observer 
from smartcard.Observer import Observable 

from smartcard.CardType import AnyCardType 
from smartcard.CardRequest import CardRequest
from smartcard.CardMonitoring import CardMonitor

import gobject
gobject.threads_init()

ATR_Leocarte = [59, 129, 128, 1, 128, 128]

def isleocarte(atr):
    if not(len(atr) == 6):
        return False
    for i in range(0,6):
        if not(atr[i] == ATR_Leocarte[i]):
            return False
    return True
    
def selectreaderfromname(name):
    rs = readers()
    for r in rs:
        if r.name == name:
            return r
    return -1
    
COMMAND = [0xFF, 0xCA, 0x00, 0x00, 0x00,]

class printobserver(Observer): 

    def __init__(self):
        self.acards = []
        pass
    
    def update(self, observable, (addedcards, removedcards)): 
        #print "added:   ", addedcards 
        #print "removed: ", removedcards
        self.acards = addedcards

class cardthread(Thread): 

    def __init__(self): 
        Thread.__init__(self) 
        self.readermonitor = CardMonitor()  
        self.observer = None
        self.work = True
        self.cardid = []
    
    def run(self):
        self.observer = printobserver() 
        self.readermonitor.addObserver(self.observer) 
        while self.work:
            if(len(self.observer.acards)):
                for ac in self.observer.acards:
                    if(isleocarte(ac.atr)):
                        print "atr léo"
                        lecteur = selectreaderfromname(ac.reader)
                        if type(lecteur) is int and lecteur == -1:
                            print "Readers not found!!!"
                        else:
                            try:
                                connection = lecteur.createConnection()
                                connection.connect()
                                data, sw1, sw2 = connection.transmit(COMMAND)
                                if( not(sw1==0x90) and not(sw2==00)  ):
                                    print "Erreur la carte n'a pas bien répondue"
                                    self.cardid.append(-2)
                                else:
                                    self.cardid.append(data)
                                    """for d in data:
                                        print hex(d),
                                    print "\t",
                                    print "Command: %02X %02X" % (sw1, sw2)"""
                            except smartcard.Exceptions.NoCardException:
                                print "Card removed ?"
                                self.cardid.append(-1)
                            except:
                                print "Unexpected error:", sys.exc_info()[0]
                                raise
                    else:
                        print "not leo"
                self.observer.acards = []
            sleep(0.25) 
        self.readermonitor.deleteObserver(self.observer)
        
class _IdleObject(gobject.GObject):
    """
    Override gobject.GObject to always emit signals in the main thread
    by emmitting on an idle handler
    """
    def __init__(self):
        gobject.GObject.__init__(self)

    def emit(self, *args):
        gobject.idle_add(gobject.GObject.emit,self,*args)
        
def convertSN(data):
    return (data[0]<<(8*6)) + (data[1]<<(8*5)) + (data[2]<<(8*4)) + (data[3]<<(8*3)) + (data[4]<<(8*2)) + (data[5]<<8) + data[6]

class GTKThread(Thread, _IdleObject):
    """
    Cancellable thread which uses gobject signals to return information
    to the GUI.
    """
    __gsignals__ =  {
            "card_long": (
                gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, [
                gobject.TYPE_ULONG]),        
            "card": (
                gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, [
                gobject.TYPE_UINT,gobject.TYPE_UINT,gobject.TYPE_UINT,gobject.TYPE_UINT,gobject.TYPE_UINT,gobject.TYPE_UINT,gobject.TYPE_UINT]),       
            "card_error": (
                gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, [])   
            }

    def __init__(self, *args):
        Thread.__init__(self)
        _IdleObject.__init__(self)
        self.work = True
        self.readermonitor = CardMonitor()  
        self.observer = None

    def stop(self):
        """
        Threads in python are not cancellable, so we implement our own
        cancellation logic
        """
        self.work = False

    def run(self):
        print "Running GTKCardmonitor Thread"
        self.observer = printobserver() 
        self.readermonitor.addObserver(self.observer) 
        while self.work:
            if(len(self.observer.acards)):
                for ac in self.observer.acards:
                    if(isleocarte(ac.atr)):
                        #print "atr léo"
                        lecteur = selectreaderfromname(ac.reader)
                        if type(lecteur) is int and lecteur == -1:
                            print "Readers not found!!!"
                        else:
                            try:
                                connection = lecteur.createConnection()
                                connection.connect()
                                data, sw1, sw2 = connection.transmit(COMMAND)
                                if( not(sw1==0x90) and not(sw2==00)  ):
                                    print "Erreur la carte n'a pas bien répondue"
                                    self.emit("card_error")
                                else:
                                    #print "carte =", data
                                    self.emit("card",data[0],data[1],data[2],data[3],data[4],data[5],data[6])
                                    #self.emit("card_long",long(convertSN(data)))
                            except smartcard.Exceptions.NoCardException:
                                print "Card removed ?"
                                self.emit("card_error")
                            except smartcard.Exceptions.CardConnectionException:
                                print "Card removed ?"
                                self.emit("card_error")
                            except:
                                print "Unexpected error"
                                self.emit("card_error")
                                raise
                    else:
                        print "not leo"
            self.observer.acards = []
            sleep(0.25) 
        self.readermonitor.deleteObserver(self.observer)
                        
        print "Stoping"
    
    
if __name__ == '__main__':
    t=cardthread()
    t.run()
    sleep(100)
    self.work = False
    t.join()