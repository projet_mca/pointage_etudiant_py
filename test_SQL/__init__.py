#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Connection directe a une basse de donnée MySQL

.. moduleauthor:: Amandin Paquet
"""

import MySQLdb

db = 0
cursor = 0



def __init__():
    """Fonction de connexion à la base de données"""
    global cursor,db
    db = MySQLdb.connect("127.0.0.1", "root", "", "projet")
    cursor = db.cursor() 


def pointage_leo(data,examen):
    """Fonction du pointage des élèves
    
    :param data: Serial de la carte
    :type data: int
    :param examen: ID de l'examen
    :type examen: int
    :return etudiant: Ligne de l'étudiant
    :type tuple:
    """
  
    exam = """SELECT id_exam FROM examens WHERE UNIX_TIMESTAMP(CURRENT_TIMESTAMP) > UNIX_TIMESTAMP(examens.date) - 108000
    AND UNIX_TIMESTAMP(CURRENT_TIMESTAMP) < UNIX_TIMESTAMP(examens.date) + 216000"""
    lines = cursor.execute(exam) 
    while True:
        row = cursor.fetchone()
        if row == None: break
        id_exam = row[0]
            
    lien = """INSERT INTO `couple_etudiant_examen`(`etudiant`, `examen`) VALUES ("""+ str(data) + """,""" + str(id_exam) +""")"""
    lines2 = cursor.execute(lien)
    
    del_doubl = """DELETE t1 FROM couple_etudiant_examen AS t1, couple_etudiant_examen AS t2 WHERE t1.id_couple > t2.id_couple
    AND t1.`examen` = t2.`examen` AND t1.`etudiant` = t2.`etudiant` """
    lines3 = cursor.execute(del_doubl)
    
    db.commit()
    
    etu = cursor.execute("SELECT * FROM `etudiants` WHERE `num_serie` = "+str(data))
    etudiant = cursor.fetchone()
    return etudiant


def etudiant_present(matiere,date,promotion):
    """Fonction listing des étudiants présents
    
    :param matiere: Matière du control
    :type matiere: str
    :param date: Date du control
    :type date: str
    :param promotion: Promotion
    :type promotion: str
    """

    select = """ SELECT nom,prenom,etupass FROM etudiants, examens, couple_etudiant_examen
    WHERE matiere = '""" + matiere + """' AND examens.date ='""" + date + """'
    AND etudiants.promotion = '""" + promotion + """' AND etudiants.num_serie = couple_etudiant_examen.etudiant
    AND examens.id_exam = couple_etudiant_examen.examen """
    lines = cursor.execute(select)
    while True:
        row = cursor.fetchone()
        if row == None: break
        print(row)

    exam = """ SELECT id_exam FROM examens WHERE matiere = '""" + matiere + """' AND examens.date ='""" + date + """'
    AND examens.promotion = '""" + promotion + """'"""
    line2 = cursor.execute(exam)
    while True:
        row = cursor.fetchone()
        if row == None: break
        id_exam = row[0]

    nb_etudiants = """SELECT COUNT(*) FROM couple_etudiant_examen WHERE examen = """ + str(id_exam)
    lines3 = cursor.execute(nb_etudiants) 
    while True:
        row = cursor.fetchone()
        if row == None: break
        print " Nombre d'etudiants presents : " + str(row[0])
        
    db.commit()

    
def add_etudiant(num_serie,nom,prenom,naissance,ref,etupass,formation,groupe,promotion):
    """
    Fonction ajouter un étudiant 
    
    :param num_serie: Numéro de série de la carte étudiante
    :type num_serie: int
    :param nom: Nom de l'étudiant
    :type nom: str
    :param prenom: Prénon de l' étudiant
    :type prenom: str
    :param naissance: Date de naissance de l' étudiante
    :type naissance: date(str - AAAA-MM-JJ HH:MM:SS)
    :param ref: Numéro de rérérence de la carte étudiante
    :type ref: str
    :param etupass: étupass
    :type etupass: int
    :param formation: Formation de l' étudiant
    :type formation: str
    :param groupe: Groupe de l' étudiant
    :type groupe: str
    :param promotion: Promotion de l' étudiant
    :type promotion: str
    """

    add_etudiant = """INSERT INTO `etudiants`(`num_serie`, `nom`, `prenom`, `naissance`, `ref`, `etupass`, `formation`, `groupe`, `promotion`)
    VALUES (""" + str(num_serie) + """,'""" + nom + """','""" + prenom + """','""" + naissance + """','""" + ref + """',""" + str(etupass) + """,'""" + formation + """'
    ,'""" + groupe + """','""" + promotion + """')"""
    try:
        line = cursor.execute(add_etudiant)
    except MySQLdb.Error as err:
      print("Erreur : {}".format(err))
    db.commit()
    
    
def del_etudiant(etupass):
    """
    Fonction supprimer un étudiant 
    
    :param etupass: étupass
    :type etupass: int
    """
    
    delete = """DELETE FROM `etudiants` WHERE etupass =""" + str(etupass)
    lines = cursor.execute(delete)

    if(lines == 0):
        print " Echec de la suppression - Etupass inconnu "
    else:
        print " Suppression reussie "
   
    db.commit()

# 

def add_examen(e_matiere,e_date,professeur,promotion):
    """Fonction ajouter un examen
    
    :param e_matiere: matière
    :type e_matiere: str
    :param e_date: étupass
    :type e_date: date(str - AAAA-MM-JJ HH:MM:SS)
    :param professeur: étupass professeur
    :type professeur: int
    :param promotion: promotion
    :type promotion: str  
    
    """

    id_professeur = 1
    prof = """ SELECT num_serie FROM professeurs WHERE nom = '""" + professeur + """'"""
    line1 = cursor.execute(prof)
    while True:
        row = cursor.fetchone()
        if row == None: break
        id_professeur = row[0]
    
    add_examen = """INSERT INTO `examens`(`matiere`, `date`, `professeur`, `etudiant`, `promotion`)
    VALUES ('""" + e_matiere + """','""" + e_date + """',""" + str(id_professeur) + """,0,'""" + promotion + """')""" 
    line2 = cursor.execute(add_examen)

    db.commit()


def add_prof(num_serie,nom,prenom,naissance,ref,etupass,emploi):
    """
    Fonction ajouter un professeur
    
    """

    add_prof = """INSERT INTO `professeurs`(`num_serie`, `nom`, `prenom`, `naissance`, `ref`, `etupass`, `emploi`)
    VALUES (""" + str(num_serie) + """,'""" + nom + """','""" + prenom + """','""" + naissance + """','""" + ref + """',""" + str(etupass) + """,'""" + emploi + """')""" 
    try:
        line = cursor.execute(add_prof)
    except MySQLdb.Error as err:
      print("Erreur : {}".format(err))

    db.commit()


    
def arret_cxn():
    """
    Fonction de déconnexion de la base de données
    """
    db.close()

if __name__ == "__main__":
    __init__()
    data = 45674223346128
    pointage_leo(data)
    matiere = "PHYSIQUE"
    date = "2015-11-16 08:18:00"
    promotion = "MCA"
    del_etudiant(214)
    etudiant_present(matiere,date,promotion)
    add_examen("SVT","2015-11-16 10:00:00","PORTY","MCA")
    #add_etudiant(4100,"test","test","1998-01-01","0125",2130,"formation","groupe","promotion")
    #add_prof(4200,"test","test","1850-12-12","0126",2131,"emploi")
    arret_cxn()
    
