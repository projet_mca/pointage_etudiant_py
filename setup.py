#!/usr/bin/env python
# -*- coding: utf-8 -*-

from cx_Freeze import setup, Executable
import os, site, sys

## Get the site-package folder, not everybody will install
## Python into C:\PythonXX
site_dir = site.getsitepackages()[1]
include_dll_path = os.path.join(site_dir, "gtk-2.0\\runtime\\bin")



## Collect the list of missing dll when cx_freeze builds the app
missing_dll = ['libgtk-win32-2.0-0.dll',
               'libgdk-win32-2.0-0.dll',
               'libatk-1.0-0.dll',
               'libcairo-gobject-2.dll',
               'libgdk_pixbuf-2.0-0.dll',
               'libjpeg-8.dll',
               'libpango-1.0-0.dll',
               'libpangocairo-1.0-0.dll',
               'libpangoft2-1.0-0.dll',
               'libpangowin32-1.0-0.dll'
               # 'libgnutls-26.dll',
               # 'libgcrypt-11.dll',
               # 'libp11-kit-0.dll'
]

## We also need to add the glade folder, cx_freeze will walk
## into it and copy all the necessary files
glade_path = os.path.join(site_dir, "gtk-2.0\\runtime\include\libglade-2.0\\")
glade_folder = 'glade'
# glade_folder = os.path.join(site_dir, "gtk-2.0\\")
# glade_folder += "runtime\include\libglade-2.0\glade"



## We need to add all the libraries too (for themes, etc..)
gtk_libs = ['etc', 'lib', 'share']

## Create the list of includes as cx_freeze likes
include_files = []
for dll in missing_dll:
    include_files.append((os.path.join(include_dll_path, dll), dll))

## Let's add glade folder and files
include_files.append((glade_path, glade_folder))

## Let's add gtk libraries folders and files
for lib in gtk_libs:
    include_files.append((os.path.join(os.path.join(site_dir, "gtk-2.0\\runtime"), lib), lib))

base = None

zip_includes=[]
"""zip_includes.append(("gui/leocarte.png","gui/leocarte.png"))
zip_includes.append(("gui/loading.glade","gui/loading.glade"))
zip_includes.append(("gui/main.glade","gui/main.glade"))
zip_includes.append(("gui/login.glade","gui/login.glade"))"""


annuler_copie = True
if annuler_copie:
    include_files = []
    
moduleperso= ("gui/leocarte.png","gui/loading.glade","gui/main.glade","gui/login.glade","gui/icon.ico","gui/icon_512.png")
for filep in moduleperso:
    include_files.append((filep,filep))

## Lets not open the console while running the app
if sys.platform == "win32":
    base = "Win32GUI"

executables = [
    Executable("main.py",
               base=base,
               icon="gui/icon.ico"
    )
]

buildOptions = dict(
    compressed=False,
    includes=['gobject', 'gtk'],
    packages=['pygtk'],
    include_files=include_files,
    excludes= ['_gtkagg', '_tkagg', 'bsddb', 'curses', 'email', 'pywin.debugger',
            'pywin.debugger.dbgcon', 'pywin.dialogs', 'tcl',
            'Tkconstants', 'Tkinter'],
    zip_includes=zip_includes
    
    )

#Logiciel qui permet de pointer les étudiants grâce à la léocarte

setup(
    name="Pointage_etudiant",
    author="Amandin Paquet - Thomas Portassau",
    version="0.0.1",
    description="Logiciel qui permet de pointer les etudiants grace a la leocarte",
    options=dict(build_exe=buildOptions),
    executables=executables
)